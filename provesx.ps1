<#
.Synopsis
	ESXi 5.x Host Provisioning Script
.DESCRIPTION
	What the script does:
		-Checks to make sure the host is in maintenance mode
		-Applies license if the -License parameter is used and there are available CPU's left in license
		-Renames local datastore to standard
		-Copy's VLANs to new host if the -ExistingVMHost parameter is used
		-Adds VLANs if the -NewVLAN parameter is used
		-Changes Management IP to static from the IP
		-Creates the VMkernel and assigns it the IP
		-Checks vSwitch configuration and configured Failover order accordingly
		-Changes esxAdminsGroup to vISP_VC_ManagedHosts_Admins
		-Sets the hostname
		-Joins to NA domain
		-Detaches host profile
		-Provides WWN to send over to the storage team
	
	What the script doesn't do:
		-Change root password
		-Remove autodeploy rule
		-Remove DHCP reservation from DPL server
		-Install updates
.PARAMETER vCenter
	Required to connect to vCenter
.PARAMETER VMHost
	Required to provision ESX host
.PARAMETER VMkernelIP
	Required to create VMkernel and set IP
.PARAMETER License
	This isn't required but you still must license the host manaually if you are leaving this parameter out
.PARAMETER ExistingVMHost
	Providing this parameter will copy all VLANs on this ExistingVMHost to VMHost
.PARAMETER NewVLAN
	List VLAN IDs separated by a comma to add to VMHost
.EXAMPLE
	.\provesx.ps1 -vCenter ab1vc03 -VMHost s12345ab1vh01 -VMkernelIP 10.10.10.10
	Provisions server s12345ab1vh01 in the ab1vc03 vCenter and assigns it a VMkernelIP of 10.10.10.10.
.EXAMPLE
	.\provesx.ps1 -vCenter ab1vc03 -VMHost s12345ab1vh01 -VMkernelIP 10.10.10.10 -License XXXXX-XXXXX-XXXXX-XXXXX-XXXXX
	Provisions server s12345ab1vh01 in the ab1vc03 vCenter and assigns it a VMkernelIP of 10.10.10.10 and a license of XXXXX-XXXXX-XXXXX-XXXXX-XXXXX.
.EXAMPLE
	.\provesx.ps1 -vCenter ab1vc03 -VMHost s12345ab1vh01 -VMkernelIP 10.10.10.10 -ExistingVMHost s12345ab1vh02.na.msmps.net
	Provisions server s12345ab1vh01 in the ab1vc03 vCenter, assigns it a VMkernelIP of 10.10.10.10 and copys all of the VLANs from s12345ab1vh02 to s12345ab1vh01.
.EXAMPLE
	.\provesx.ps1 -vCenter ab1vc03 -VMHost s12345ab1vh01 -VMkernelIP 10.10.10.10 -NewVLAN 1000,1001,1002,1003,1004
	Provisions server s12345ab1vh01 in the ab1vc03 vCenter, assigns it a VMkernelIP of 10.10.10.10 and adds all of the VLANs listed.  The VLANs get created with the cS12345_XXXX naming convention.
.LINK
	https://confluence.savvis.net/display/~Michael.Klebolt/ESXi+Server+Build+Script
.NOTES
	AUTHOR  : Mike Klebolt, CenturyLink Technology Solutions
	DATE	: 2016-03-21
	VERSION : 1.01.00
			   |  |  +- Bug fix version
			   |  +---- Feature version
			   +------- Major version
	CHANGELOG
		1.00.00 - Initial Release
		1.01.00 - Added ability to store password as global variable.  This will prevent getting prompted for password every time the script is ran as long as that powershell window remains open.

#>
[CmdletBinding()]
Param
(
	[Parameter(Mandatory=$true,Position=0)]
	[string]$vCenter,
	
	[Parameter(Mandatory=$true,Position=1)]
	[string]$VMHost,
	
	[Parameter(Mandatory=$true,Position=3)]
	[string]$VMkernelIP,
	
	[Parameter(Mandatory=$false,Position=4,HelpMessage="License to use on host")]
	[string]$License,

	##Asking for NA password to be able to join host to domain.  Computer account must exist already.
	#[Parameter(Mandatory=$true,Position=5)]
	#[Security.SecureString]$NAPassword,
	
	[Parameter(Mandatory=$false,Position=6,HelpMessage="If you're wanting to add all the VLANs from an existing host in a cluster, provide the existing VMHost name here.")]
	[string]$ExistingVMHost,
	
	[Parameter(Mandatory=$false,Position=7,HelpMessage="Add new VLANs to host.  Only VLANID is required as the script will format the VLAN Name to cS12345_1111 naming convention.")]
	[string[]]$NewVLAN	

	
)
#Checks to see if $NAPassword variable exists, if not stores it as a global variable so you aren't prompted for it again if the script is ran multiple times.
Function Get-NAPassword
{
	If ($NAPassword -eq $null)
	{
		Set-Variable -Name NAPassword -scope Global -Value (Get-credential na-msmps\$env:username -Message "Please enter your NA Password.") 
		Write-Host "Storing password as a global variable so you won't have to type it in again as long as this powershell window remains open.`n" -foregroundcolor green
	}
}


#Adding PowerCLI snapin in case the script is ran in powershell instead of PowerCLI
Add-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue

Function Validate-Params
{
	If ($VMHost -eq "" -or $VMHost -match '(na.msmps.net)')
	{
		Write-Host "VMHost parameter contains na.msmps.net. Please use short hostname.  Terminating script.`n" -foregroundcolor red
		Exit
	
	}

	If ($VMkernelIP -eq "" -or $VMkernelIP -notmatch '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
	{
		Write-Host "No VMkernelIP specified or invalid IP format. Terminating script.`n" -foregroundcolor red
		Exit
	}

	If ($vCenter -eq "")
	{
		Write-Host "No vCenter specified. Terminating script.`n" -foregroundcolor red
		Exit
	}

	try
	{
		Get-ADComputer $VMHost -ErrorAction Stop | Out-Null
	}
	catch
	{
		Write-Host "$VMHost Computer object NOT found in Active Directory.  Terminating script." -ForegroundColor red
		Exit
	}
	If ((Get-ADComputer $VMHost -ErrorAction Stop).DistinguishedName -Match "CN=Computer")
	{
		Write-Host $VMHost "Computer account exists in AD but CreateClientOU has NOT been ran.  Terminating script." -foregroundcolor red
		Exit
	}
}
	
Function Connect-vCenter
{
	Write-Host "Connecting to $vCenter.`n" -foregroundcolor Green
	Connect-VIServer $vCenter -ErrorAction Stop -WarningAction SilentlyContinue | Out-Null
}


Function Check-MaintenanceMode
{
	#Added this as a safety check.  We don't want to take the chance of having changes made on a potentially live host.
	Write-Host "Checking to see if $VMHost is in Maintenance Mode.`n" -foregroundcolor green
	if ($v.ConnectionState -eq "Maintenance")
	{
		Write-Host "$VMHost is in Maintenance Mode. Proceeding with script.`n" -foregroundcolor Green
	}
	else
	{
		Write-Host "$VMHost is NOT in Maintenance Mode. Were the pre-reqs completed? Are you sure you have the correct hostname? Terminating script.`n"  -foregroundcolor red
		Exit
	}
}

Function License-VMHost
{
$servInst = Get-View ServiceInstance
$licenceMgr = Get-View $servInst.Content.licenseManager
$licenses = $licenceMgr.Licenses | where {$_.LicenseKey -eq $License}
$HostCPU = $v.ExtensionData.Summary.Hardware.NumCpuPkgs  

	If ($License -eq "")
	{
		Write-Host "License not provided.  Please ensure the license gets added to the host manually or you run the script again with the License parameter.`n" -foregroundcolor yellow
	}
	ElseIf ($v.LicenseKey -match $License)
	{
		Write-Host "$License already applied to $VMHost. Skipping...`n"  -foregroundcolor Green
		If ($licenses.Used -gt $licenses.Total)
		{
			Write-Host "Please note that $License is over its CPU Limit.  Please request a new license for this host`n" -foregroundcolor yellow
		}
	}
	ElseIf (($licenses -eq $null) -or ((!($licenses.Total-$licenses.Used -lt $HostCPU)) -and ($v.LicenseKey -eq "00000-00000-00000-00000-00000")))
	{
			Write-Host "Applying License $License to $VMHost`n"  -foregroundcolor Green
			Set-VMHost -VMHost $v -LicenseKey $License -ErrorAction Stop -WarningAction SilentlyContinue  | Out-Null
	}
	ElseIf ($licenses.Total-$licenses.Used -lt $HostCPU)
	{
		Write-Host "This License key has reached its limit.  Please request a new license that has available CPUs" -foregroundcolor Red
		Exit
	}	
}

Function Rename-LocalDatastore
{
	Write-Host "Renaming local datastore to local-$VMHost if it hasn't been done already.`n"  -foregroundcolor Green
	$v | get-datastore | where {$_.name -match "datastore"} | set-datastore -name local-$VMHost  -ErrorAction Stop -WarningAction SilentlyContinue | Out-Null
}

Function Add-VLAN
{
	If ($NewVLAN -eq "")
	{
		Return
	}
	#Vague check for vSwitch1 presence here since we already did a thorough one in the Set-FailoverPolicy function
	Write-Host "Checking to see if VLANs exist already, if not creating them.`n" -foregroundcolor green
	if ($vswitch1 -eq $null)
	{
		$switch = "vSwitch0"
	}
	else
	{
		$switch = "vSwitch1"
	}
	ForEach ($VLAN in $NewVLAN) 
	{
		$VLANName = "c" + $VMHost.Substring(0,7).ToUpper() + "_" + $VLAN

		if (!($v | Get-VirtualSwitch -Name $switch | Get-VirtualPortGroup | Where {$_.VLANID -eq $VLAN} -ErrorAction SilentlyContinue))
		{
			write-host "Creating portgroup" $VLANName -foregroundcolor green
			$v | Get-VirtualSwitch -Name $switch |New-VirtualPortgroup -Name $VLANName -VLanId $VLAN | out-null
		}
		else 
		{ 
			write-host "A portgroup with the VLANID of" $VLAN "already exists.  Skipping...`n"  -foregroundcolor green
		}
	}
}



Function Copy-VLANS
{
	If ($ExistingVMHost -eq "")
	{
		#Write-Host "Existing VMHost not provided, not copying VLANs.`n" -foregroundcolor yellow
		return
	}
	
	
	If(!($Source_Host = get-vmhost $ExistingVMHost -erroraction SilentlyContinue)) 
	{ 
		Write-Host $ExistingVMHost "does not exists! Please ensure your ExistingVMHost hostname is correct.  FQDN is required for this parameter.`n"
		exit
	}
	#Vague check for vSwitch1 presence here since we already did a thorough one in the Set-FailoverPolicy function
	if ($vswitch1 -eq $null)
	{
		$switch = "vSwitch0"
	}
	else
	{
		$switch = "vSwitch1"
	}
	$Source_portgroups = $Source_Host | get-virtualswitch -Name $switch | get-virtualportgroup
	$Dest_vSwitch = Get-VirtualSwitch -VMHost $v -Name $switch
	Foreach ($pgr in $Source_Portgroups)
	{
		if (!( $v | Get-VirtualPortgroup -Name $pgr.Name -ErrorAction SilentlyContinue))
		{
			Write-Host "Creating portgroup" $pgr.Name -foregroundcolor green
			$v | Get-Virtualswitch -Name $switch | New-VirtualPortgroup -Name $pgr.Name -VLanId $pgr.VLanId | out-null
		}
		else 
		{ 
			write-host "The portgroup" $pgr.Name "already exists." -foregroundcolor green
		}
	}
}


Function Set-ManagementIP
{
	Write-Host "Changing $ManagementIP from dynamic to static if it hasn't already been done.`n"  -foregroundcolor Green
	$v | Get-VMHostNetworkAdapter -Name vmk0 | Set-VMHostNetworkAdapter -IP $ManagementIP -SubnetMask $Subnet -confirm:$false  -ErrorAction Stop -WarningAction SilentlyContinue   | Out-Null
}

Function Set-VMkernelIP
{
	Write-Host "Checking to see if VMkernel has been configured already.`n" -foregroundcolor green
	If (!($vmk1.Name.Contains("vmk1")))
	{
		Write-Host "Creating VMkernel PortGroup and assigning IP $VMkernelIP and subnet $Subnet.`n"  -foregroundcolor Green
		New-VMHostNetworkAdapter -VMHost $v -PortGroup VMkernel -VirtualSwitch vSwitch0 -IP $VMkernelIP -SubnetMask $Subnet -VMotionEnabled:$true -Confirm:$false -ErrorAction Stop -WarningAction SilentlyContinue   | Out-Null
	}
	If ($vmk1.Name -eq "vmk1" -and $vmk1.DhcpEnabled -eq $true)
	{
		Write-Host "VMkernel already exists but needs IP configured.  Configuring IP $VMkernelIP and subnet $Subnet.`n" -foregroundcolor Green
		$v | Get-VMHostNetworkAdapter -Name vmk1 | Set-VMHostNetworkAdapter -IP $VMkernelIP -SubnetMask $Subnet -confirm:$false  -ErrorAction Stop -WarningAction SilentlyContinue   | Out-Null
	}
	else
	{
		Write-Host "Skipping VMkernel creation as it appears to be already configured.`n" -foregroundcolor green
	}
}

Function Set-esxAdminsGroup	
{
	Write-Host "Setting esxAdminsGroup to vISP_VC_ManagedHosts_Admins.`n"  -foregroundcolor Green
	$v | Get-AdvancedSetting -Name Config.HostAgent.plugins.hostsvc.esxAdminsGroup | Set-AdvancedSetting -value "vISP_VC_ManagedHosts_Admins" -Confirm:$false -ErrorAction Stop -WarningAction SilentlyContinue  | Out-Null

}

Function Set-Hostname
{
	Write-Host "Setting hostname to $FQDN.`n"  -foregroundcolor Green
	$esxcli.system.hostname.set($null,"$FQDN",$null)   | Out-Null
}

Function Set-FailoverPolicy
{	
	$check_vswitch1 = (($nics | where {$_.extensiondata.linkspeed.speedmb -eq 10000}).count -ne 2)
	Write-Host "Checking vSwitch configuration to see if its 4/8, or 4/4 or 2/4.`n" -foregroundcolor green
	if ($nics | where {$_.name -eq 'vmnic7'})
	{
		# 4/8 configuration
		if (! ($vswitch0.Nic.count -eq 2 -and $vswitch0.Nic.Contains("vmnic0") -and $vswitch0.Nic.Contains("vmnic7")))
		{
			Write-Host "4/8 nic configuration detected but vSwitch0 does not contain both vmnic0 and vmnic7.  Terminating script.`n" -foregroundcolor red
			Exit
		}
		
		if (! ($vswitch1.Nic.count -eq 2 -and $vswitch1.Nic.Contains("vmnic1") -and $vswitch1.Nic.Contains("vmnic6")))
		{
			Write-Host "4/8 nic configuration detected but vSwitch1 does not contain both vmnic1 and vmnic6.  Terminating script.`n" -foregroundcolor red
			Exit
		}	
		Write-Host "Setting Failover Policy to 4/8 nic configuration for VMkernel`n"  -foregroundcolor Green
		$esxcli.network.vswitch.standard.portgroup.policy.failover.set("vmnic7",$null,$null,$null,$null,"VMkernel","vmnic0",$null) | Out-Null
	}
	else
	{
		# Both 2/4 and 4/4 have the same vswitch0 config
		if (! ($vswitch0.Nic.count -eq 2 -and $vswitch0.Nic.Contains("vmnic0") -and $vswitch0.Nic.Contains("vmnic3")))
		{
			Write-Host "2/4 or 4/4 nic configuration detected but vSwitch0 does not contain only vmnic0 and vmnic3" -foregroundcolor red
			Exit
		}
		
		# 2/4 does not have vswitch1
		if ($check_vswitch1)
		{
			if (! ($vswitch1.Nic.count -eq 2 -and $vswitch1.Nic.Contains("vmnic1") -and $vswitch1.Nic.Contains("vmnic2")))
			{
				Write-Host "2/4 nic configuration detected but vSwitch1 does not contain only vmnic1 and vmnic2" -foregroundcolor red
				Exit
			}
		}
	Write-Host "Setting Failover Policy to 2/4 or 4/4 for VMkernel`n"  -foregroundcolor Green
	$esxcli.network.vswitch.standard.portgroup.policy.failover.set("vmnic3",$null,$null,$null,$null,"VMkernel","vmnic0",$null) | Out-Null
	}

}

Function Join-Domain
{
	Write-Host "Checking to see if $VMHost has been joined to NA domain.`n" -foregroundcolor green
	If (!($domain.Domain -eq "na.msmps.net"))
	{
		Write-Host "Joining NA Domain.`n" -foregroundcolor green
		Get-VMHostAuthentication -VMHost $v | Set-VMHostAuthentication -JoinDomain -Domain na.msmps.net -User $env:username -Password $NAPassword.password -confirm:$false -ErrorAction Stop -WarningAction SilentlyContinue | Out-Null
	}
	else
	{
		Write-Host "$VMHost appears to be joined to the NA domain already. Skipping...`n" -foregroundcolor green
	}
}

Function Detach-HostProfile
{
	Write-Host "Detaching host profile from $VMHost if it hasn't already been detached.`n" -foregroundcolor green
	if ($HostProfile.Name -eq $null)
	{
		Write-Host "No host profile attached. Skipping...`n" -foregroundcolor green
	}
	else
	{
		Remove-VMHostProfile -Entity $v -confirm:$false | out-null
	}
}

Function Get-WWN
{
	Write-Host "Here are the WWNs to provide to the storage team.`n" -foregroundcolor green
	#$v | Get-VMHostHBA -Type FibreChannel | Select VMHost,Device,@{N="WWN";E={"{0:X}" -f $_.PortWorldWideName}} | Sort VMhost,Device | Format-Table -AutoSize
	$v | Get-VMHostHBA -Type FibreChannel | Select VMHost,Device,@{N="WWN";E={((("{0:X}"-f $_.PortWorldWideName).ToLower()) -replace "(\w{2})",'$1:').TrimEnd(':')}} | ft -AutoSize

}

Function Random-Password ($length = 10)
{
$punc = 46..46
    $digits = 48..57
    $letters = 65..90 + 97..122
    $password = get-random -count $length `
        -input ($punc + $digits + $letters) |
            % -begin { $aa = $null } `
            -process {$aa += [char]$_} `
            -end {$aa}
	Write-Host "Here is a randomly generated password you can use this for the root password:" $password
}

$FQDN = $VMHost + ".na.msmps.net"
Get-NAPassword
Validate-Params
Connect-vCenter
#Declaring more variables now that we're connected to vCenter
Write-Host "Setting environment variables now that we're connected to vCenter.`n" -foregroundcolor green
$v = Get-VMHost $FQDN -ErrorAction stop
$HostProfile = $v | Get-VMHostProfile
$vswitch = $v | Get-VirtualSwitch
$vswitch0 = $vswitch | where {$_.Name -eq "vSwitch0"}
$vswitch1 = $vswitch | where {$_.Name -eq "vSwitch1"}
$nics = $v | Get-VMHostNetworkAdapter -physical
$ManagementIP = ($v | Get-VMHostNetworkAdapter -Name vmk0).IP
$Subnet = ($v | Get-VMHostNetworkAdapter -Name vmk0).SubnetMask
$vmk1 = Get-VMHostNetworkAdapter -VMHost $v
$esxcli = Get-EsxCli -VMhost $v -ErrorAction Stop -WarningAction SilentlyContinue 
$domain = Get-VMHostAuthentication -VMHost $v
Check-MaintenanceMode
License-VMHost
Rename-LocalDatastore
Copy-VLANS
Add-VLAN
Set-ManagementIP
Set-VMkernelIP
Set-FailoverPolicy
Set-esxAdminsGroup
Set-Hostname
Join-Domain
Detach-HostProfile
Get-WWN
Write-Host "`nAll done with $VMHost.`n
Please ensure all post-script requirements get completed.`n" -foregroundcolor yellow
Random-Password

